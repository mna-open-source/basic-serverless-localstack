'use strict';

const logger = require('pino')();
const { apiGatewayResponse } = require('./resources/apiGatewayResponse');

/**
 * API Gateway basic handler
 * @param {Object} event
 * @param {{functionName: String}} context
 * @returns {Promise<{body: string, statusCode: number}>}
 */
module.exports.hello = async (event, context) => {
    const child = logger.child({ scopeName: context.functionName, scopeType: 'handler', source: 'api-gateway' });
    child.info(`The function ${context.functionName} was called!`);

    return apiGatewayResponse(
      200,
        {
            message: 'Go Serverless v1.0! Your function executed successfully!',
            input: event,
        });
};

const even = (record) => JSON.parse(record.body).demo === 1;

/**
 * SQS basic Handler
 * @param {{Records: Array<{body: string}>}} event
 * @param {{functionName: String}} context
 * @param callback
 * @returns {Promise<void>}
 */
module.exports.world = async (event, context, callback) => {
    const child = logger.child({ scopeName: context.functionName, scopeType: 'handler', source: 'sqs' });
    child.info(`The function ${context.functionName} was called!`);

    if (event.Records.some(even)) {
        callback();
    } else {
        callback(new Error('ouch!'));
    }
};
