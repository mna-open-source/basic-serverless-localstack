# Serverless + Localstack

## Prerequisite Software

Before you can work with this project, you must install and configure the following products on your development machine:

- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) on Mac computers, it's easier to install it using Homebrew: `brew install awscli`
- [Git](http://git-scm.com) and/or the **GitHub app** (for [Mac](http://mac.github.com) or [Windows](http://windows.github.com))
- [Node.js](http://nodejs.org) - **Current version** _v12_
- [Serverless](https://www.serverless.com/framework/docs/getting-started/)
- [Docker](https://docs.docker.com/engine/install/)
- [Yarn](https://yarnpkg.com/getting-started/install)

It is recommendable to install node via [NVM](https://github.com/nvm-sh/nvm)

## Configure AWS Locally

Create your local profile:

```shell
# Set aws profile
> aws configure sso --profile localstack
```

Fill with the following information:

```shell
AWS Access Key ID [None]: test
AWS Secret Access Key [None]: test
Default region name [None]: us-east-1
Default output format [None]: json
```

To verify that the profile was created correctly

```shell
cat ~/.aws/credentials
```

You should see the following content

```shell
[localstack]
aws_access_key_id = test
aws_secret_access_key = test
```

## Run LocalStack

Run the docker container with following command: 

```shell
docker-compose up
```

To check all the services:

```shell
curl http://localhost:4566
curl http://localhost:4566/health
```

To stop all services

```shell
docker-compose down
```

## Queues: Basic usage

Run the following commands to create required queues:

```shell
aws --endpoint-url=http://localhost:4566 sqs create-queue --profile localstack --queue-name main_queue
aws --endpoint-url=http://localhost:4566 sqs create-queue --profile localstack --queue-name main_dlq
```

To check active queues:

```shell
aws --endpoint-url=http://localhost:4566 sqs list-queues --profile localstack
```

To send message:

```shell
aws --endpoint-url=http://localhost:4566 sqs send-message --profile localstack --queue-url http://localhost:4566/000000000000/main_queue --message-body '{"demo": 1}'
```

To receive message:

```shell
aws --endpoint-url=http://localhost:4566 sqs receive-message --profile localstack --queue-url http://localhost:4566/000000000000/main_queue
aws --endpoint-url=http://localhost:4566 sqs receive-message --profile localstack --queue-url http://localhost:4566/000000000000/main_dlq
```

To purge queues:

```shell
aws --endpoint-url=http://localhost:4566 sqs purge-queue --profile localstack --queue-url http://localhost:4566/000000000000/main_queue
aws --endpoint-url=http://localhost:4566 sqs purge-queue --profile localstack --queue-url http://localhost:4566/000000000000/main_dlq
```

## SSM

Run the following commands to create required parameters:

```shell
aws --endpoint-url=http://localhost:4566 ssm put-parameter --profile localstack --name "/arn/main_queue" --value "arn:aws:sqs:us-east-1:000000000000:main_queue" --type "String" --overwrite
aws --endpoint-url=http://localhost:4566 ssm put-parameter --profile localstack --name "/arn/main_dlq" --value "arn:aws:sqs:us-east-1:000000000000:main_dlq" --type "String" --overwrite
```

To check active parameters:

```shell
aws --endpoint-url=http://localhost:4566 ssm get-parameters-by-path --path "/arn" --profile localstack --recursive
```

## Serverless: Basic usage

Deploy functions:

```shell
serverless deploy --stage local
```

To verify that the functions were created correctly:

```shell
serverless info --stage local
aws --endpoint-url=http://localhost:4566 apigateway get-rest-apis --profile localstack
aws --endpoint-url=http://localhost:4566 apigateway get-resources --rest-api-id {itemId} --profile localstack

curl http://localhost:4566/restapis/{id}/local/_user_request_/hello
```

To watch logs for every function:

```shell
serverless logs -f hello -s local -t
```

## Test functions

To test the function that is invoked by API Gateway, just execute the following command:

```shell
curl http://localhost:4566/restapis/{itemId}/local/_user_request_/hello
```

To test the function that is invoked by SQS, just execute the following command:

```shell
aws --endpoint-url=http://localhost:4566 sqs send-message --profile localstack --queue-url http://localhost:4566/000000000000/main_queue --message-body '{"demo": 1}'
```

Verify that the message from the queue has been consumed and that there are no messages in the DLQ:

```shell
aws --endpoint-url=http://localhost:4566 sqs receive-message --profile localstack --queue-url http://localhost:4566/000000000000/main_queue
aws --endpoint-url=http://localhost:4566 sqs receive-message --profile localstack --queue-url http://localhost:4566/000000000000/main_dlq
```

## Links

- [Docker Compose LocalStack](https://onexlab-io.medium.com/docker-compose-localstack-fadee1e88a49)
- [Serverless LocalStack Lambda API Gateway](https://onexlab-io.medium.com/serverless-localstack-lambda-api-gateway-7fad63d9a9eb)
- [Websocket](https://www.serverless.com/framework/docs/providers/aws/events/websocket/)
- [Simple application with API Gateway Websockets | Serverless](https://www.youtube.com/watch?v=Quk_XHMvFJI)
