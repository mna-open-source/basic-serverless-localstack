'use strict';

module.exports.apiGatewayResponse = (statusCode, body) => {
    return {
        statusCode,
        body: JSON.stringify(
            body,
            null,
            2
        ),
    }
}
